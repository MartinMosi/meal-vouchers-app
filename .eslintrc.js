module.exports = {
  parser:  '@typescript-eslint/parser',  // Specifies the ESLint parser
  extends:  [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',  // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    'prettier/@typescript-eslint',  // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    'plugin:prettier/recommended',  // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
  ],
  plugins: [
    'react-hooks',
    'functional'
  ],
  parserOptions:  {
    project: "./tsconfig.json",
    ecmaVersion:  2018,  // Allows for the parsing of modern ECMAScript features
    sourceType:  'module',  // Allows for the use of imports
    ecmaFeatures:  {
      jsx:  true,  // Allows for the parsing of JSX
    },
  },
  rules:  {
    'no-console': [2, { allow: ["log", "error", "warn"]}],
    'react/prop-types': 0,
    'react-hooks/rules-of-hooks': 'error',
    "react-hooks/exhaustive-deps": "warn",

    '@typescript-eslint/no-explicit-any': 2,
    '@typescript-eslint/explicit-function-return-type': 0,
    "@typescript-eslint/interface-name-prefix": [2, "always"],
    '@typescript-eslint/array-type': [2, { default: 'generic' }], // TODO remove 'generic' once new babel will be available
    '@typescript-eslint/no-unused-vars': [2, {
      argsIgnorePattern: "^_"
    }],

    // functional rules
    "functional/no-let": 2,
    "functional/prefer-readonly-type": 2,
    "functional/immutable-data": [2, { ignorePattern: "(^mutable|^this\.mutable)"}],

    // Disabled rules
    '@typescript-eslint/no-use-before-define': 0,
  },
  env: {
    browser: true,
    node: true,
    jest: true,
  },
  settings:  {
    react:  {
      version:  'detect',  // Tells eslint-plugin-react to automatically detect the version of React to use
    },
  },
};
